Presented is an Open Source Freeware Unity Addon by DynaZor

# DynaZor's DynaTools

[main-project]:https://bitbucket.org/DynaZor/dynatools/	"DynaZor's DynaTools"
[semver]:https://bitbucket.org/DynaZor/dynatools/src/master/SemVer.md	"Semantic Versioning"
[issues]: https://bitbucket.org/DynaZor/dynatools/issues/	"Issues"



## About

An asset made to ease casual development with Unity.

**Quick Tools for Quick Unity Users**

**Easy to See and Use Interface**

## Table of Contents

[TOC]



## Install

Open your Unity project in Unity.

Then go to "Window \ Package Manager"

in the new window click the "+" button,

choose "Git" and paste this in:

```
https://bitbucket.org/DynaZor/dynatools.git
```



## Features

**As of v0.01.000**

([Back to Table Of Contents](#markdown-header-table-of-contents))

### Big Colored Editor State Label

Easily see the editor's state.

No more looking at the edges of the window and guessing what's Unity doing.



### Scene Shortcuts

**Load** (Normal\Additive) and **Play** Scenes from user-defined lists.



### Quickly Set Unity's Project Settings

Quickly change the project's Name, Company, Dev-Stage and Version.

No need to dig into Project Settings anymore ~,~

And you only need to write the information ONCE!

The addons will fill the details in the right formats everywhere they're needed.



([Back to Table Of Contents](#markdown-header-table-of-contents))



### Big Play\Stop Button

Harder to miss, easier to find and Multi Purpose!

Integrated with the DynaTools' scene preloading system.



### Additive Preloading a Scene Before Play

Now you don't need to remember to add that scene before play,

just click the Play button ;)

There's also a "Preload" button for just additively loading the Preload Scene without playing.

Set the desired Preload Scene in the Settings.



### Quick Build and No Accidental Overwrites!

Build by filling in the version, dev-stage and clicking "Build".

You can also choose from a number of Folder Naming options;

[Date]  [Version]  [State]\_[Version]  [Version]\_[Date]  [Date]\_[Version]

Set the desired build location and versioning preferences in the Settings.

Also, if there's already a folder with the same name,

then Editor Utilities will add a letter to the end of the build name and increment it until there isn't a folder with that name.



([Back to Table Of Contents](#markdown-header-table-of-contents))



### Versioning Control + Semantic Versioning + Dev-Stage

I wrote and implemented a library for

easy, user-defined and standardized versioning control.

Also, added is a Development Stage;

Mark the build as Test, POC (Proof of Concept), Prototype, Alpha, Beta, Final.



### Never forget giving credit for assets again!

Use the "DynaZor\Used Assets" tool to log other devs' assets for crediting and contact purposes.



### Recent Projects Editor

Clean the Projects appearing in Unity's Recent Projects.



### Create C# Solution

Sometimes Unity won't make the project files needed for IDE's to work with the project,
Fix a headache inducing issue in one click.



### Additional Small Features

- Advanced Information: Get Unity's version and its C# version
- Last Build - See last build's Folder name and Dev-Stage



([Back to Table Of Contents](#markdown-header-table-of-contents))



## Additional Usage Examples

* Reference the assigned App Settings asset and read its info into your project. 

    (Currently Editor Only, use Right Click - Get from Editor Utilities)

    ```Code:
    AppSettings.Current
    ```

    Example 1: Show the Version and Dev-Stage on the game's Main Menu.

    Example 2: For a project with a name changing a lot, show the Project Name on the Main Menu.

     

* *Have ideas for additional usage of this asset?*

    *Suggest it in the [**Issues**](issues) page!*



([Back to Table Of Contents](#markdown-header-table-of-contents))



## Planned Features

* **Quick Build Settings: "v Prefix" Checkbox**

* **Unity Layout Switching**

* **Quick Build: Custom Presets**

* **User Profiles** + Sub Profiles

    (currently no "v" prefix is added to build folder name before the version number)

* **Tools Dropdown**: Recents Editor, Unity Project Settings, Create C# Project Files

* Quick Build: Build Name Format: Custom Formatting

* Version Control: Changelog Generator + Version Counter (Auto Increment)

* Editor State Settings: Custom Colors

    ​     

* **Have an idea for an additional feature everyone will enjoy?**

    *Suggest it in the [**Issues**](issues) page!*



([Back to Table Of Contents](#markdown-header-table-of-contents))



## Known Issues

* Scene Shortcuts: Only some (maybe only active) scenes have their Build Index

* AppSettings.ActiveAsset is null accessible on Play and Build

    *Workaround: Make an AppSettings variable in a singleton GameObject,* 

    *set from the editor by Right Click - Get from Editor Utilities*

      

* *Found issues with the addon?*

    *Please report them in the [**Issues**](issues) page :)*



([Back to Table Of Contents](#markdown-header-table-of-contents))

## Versioning

We use [SemVer](http://semver.org/) for versioning.

See **[SemVer.md][semver]** for more information about Versioning.



([Back to Table Of Contents](#markdown-header-table-of-contents))



## Additional Credits

The addon, until v0.00.006, was based upon the original "Unity Editor Utilities" by Vitaliy Zasadnyy ([Link](https://assetstore.unity.com/packages/tools/utilities/unity-editor-utilities-37101)),
But since has been completely overhauled.



## Change Log

**v0.01.000**

- [NOTICE] New Name: DynaZor's DynaTools
- [NOTICE] Incompatible with previous versions' settings, will reset. Won't handle old settings assets.
- [Upgrade] Play Button > Play\Stop Button
- [Upgrade+BugFix] Tool usable without AppSettings
- [Tweak] Visual: Shorter and Smaller Errors\Warnning
- [Tweak] Visual; Fixed Cut Text
- [Tweak] Visual: Red Labels for problematic fields
- [UX Tweak] Default Version now is 0.00.001 Prototype
- [Backend] Partial Code Cleanup
- [Backend] Better Var Names
- [Backend] Some refactoring
- [Note To Myself] Need Code Clean (from no-settings-run-try)



([Back to Table Of Contents](#markdown-header-table-of-contents))



**v0.00.006**

* AppSettings Property Drawer: Right Click - Get from Editor Utilities: Now works with private serialized variables, too
* Scene Shortcuts\Play - Prompt Save if there are Unsaved Scenes



([Back to Change Log Title](#markdown-header-change-log))

([Back to Table Of Contents](#markdown-header-table-of-contents))



**v0.00.005**

* New: Version Consistency Check between addon's files
* New: AppSettings property drawers now have Context Menu containing two options:
    * "Get from Editor Utilities" - to reference the Editor Utilities' Current AppSettings (var needs to be public!)
    * "Apply Information to Project" - Appears only when selected AppSettings' information is different from the project's
* New: Different (sub) Namespaces for differentiating parts of Editor Utilities
* New: More Info panel in Settings showing Unity and C# versions, Detected System Language
* New: "Recent Projects Editor" Toolbar Button
* New: Warnings\Errors for missing needed AppSettings properties
* New: Right Click Button Functions:
    * Play Button: Right Click to Stop
    * Preload Button: Right Click to Unload
    * Build Button: Right Click to Build To... (Browse)
    * Scene Lists: "+" Button: Right Click to Unload
* New: Scene Management works only when not playing (can change in Advanced Settings)
* New: Base.cs: Helpers for Play Mode (Currently only EU Debug)
* New: EUExtensions: Get and Set SerializedProperty easily
* New: EUEditorOnly: Editor Only Helper Methods for Editor Utilities
* Script Refactoring and Maintenance



([Back to Change Log Title](#markdown-header-change-log))

([Back to Table Of Contents](#markdown-header-table-of-contents))



**v0.00.004**

* New: Quick Build Settings + Quick Build Target + Persistent Quick Build Settings 

    Including: Open Folder, Auto Dev, Always Dev, Unity's Build Options, Target Selection

* New: Create and Auto Reference "App Settings" Asset from the Settings panel

* Easier Scene Referencing in Settings

* Settings GUI Overhaul: Now categorized and collapsible

* Settings: Build Path: Browse to set path

* AppSettings Implementation Overhaul: Now without SerializedObject

* **(Editor Only)** New: Static reference to the assigned AppSettings through 

    ```Code:
    AppSettings.Current
    ```

* Window Refactoring

* SemVer Refactoring

* Label fixes

* Minor fixes



([Back to Change Log Title](#markdown-header-change-log))

([Back to Table Of Contents](#markdown-header-table-of-contents))



**v0.00.003**

* GUI Overhaul: Expanding width and height, Launch and Load Scene side by side,

    Launch and Load Scene scroll bars, Whole Panel Min Size and Scroll Bars, 

    Better Sizing and Placement of Play, Preload and Editor State, 

    New Interface is now capable of being super small

* New: Folder Naming Format: [Date]\_[Version]
* New: Advanced Settings: Currently only Visual Adjustments 
* New: Custom Inspector for AppSettings Assets
* Fix: Last Build Version
* Tidied up and Organized the Scripts better
* Moved "Edit Recent Projects" to Window \ Editor Utilities \ Edit Recent Projects
* Changed Namespace to DynaZor.EditorUtilities to differentiate from original
* Small fixes
* Created this README.md



([Back to Change Log Title](#markdown-header-change-log))

([Back to Table Of Contents](#markdown-header-table-of-contents))