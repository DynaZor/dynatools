﻿// Library made by Shahar DynaZor Alon, please give credit if used in a project.
using UnityEngine;
using UnityEditor;
using DynaZor.DynaTools.SemanticVersioning;
using System;

namespace DynaZor.DynaTools
{
	[System.Serializable]
	[CreateAssetMenu(fileName = "Settings", menuName = "App Settings", order = 51)]
	public class AppSettings : ScriptableObject
	{
		#region AddonInformation
		[SerializeField] public string AddonName;
		[SerializeField] public string AddonVersion;
		[SerializeField] public string AddonBy;
		#endregion

		#region Static Fields
		public static AppSettings ActiveAsset;
		#endregion

		#region enums
		public enum DevStage
		{
			Prototype, POC, Alpha, Beta, Final
		}

		public enum BuildNameFormat
		{
			Date, Version, State_Version, Version_Date, Date_Version
		}
		#endregion

		#region Global
		[SerializeField]
		public string Info_AppName;
		[SerializeField]
		public string Info_CompanyName;
		[SerializeField]
		public string Info_Copyright;
		#endregion

		#region MainWindow
		[SerializeField] public string Last_BuildName;
		[SerializeField] public DevStage Last_AppDevStage;

		[SerializeField] public BuildNameFormat Build_NameFormat;
		[SerializeField] public DevStage Ver_AppDevStage;
		#endregion

		#region SettingsWindow
		[SerializeField]
		public string Build_EXEName = "Run";
		[SerializeField]
		public int Ver_MinMajorDigits = 1;
		[SerializeField]
		public int Ver_MinMinorDigits = 2;
		[SerializeField]
		public int Ver_MinPatchDigits = 3;
		[SerializeField]
		public bool Settings_SingleInstance = true;
		[SerializeField] public string Build_RootPath = "C:\\UnityBuilds\\";
		#endregion

		#region Versions
		[SerializeField] public VersionNumber Ver_Current => new VersionNumber(Ver_Major, Ver_Minor, Ver_Patch);
		[SerializeField]
		public int Ver_Major = 0;
		[SerializeField]
		public int Ver_Minor = 0;
		[SerializeField]
		public int Ver_Patch = 1;

		[SerializeField] public VersionNumber LastBuildVersion => new VersionNumber(Ver_LastMajor, Ver_LastMinor, Ver_LastPatch);
		[SerializeField]
		public int Ver_LastMajor = 0;
		[SerializeField]
		public int Ver_LastMinor = 0;
		[SerializeField]
		public int Ver_LastPatch = 0;

		public static string VersionToString (AppSettings a, bool last = false)
		{
			if (last)
				return a.LastBuildVersion.String(a.Ver_MinMajorDigits, a.Ver_MinMinorDigits, a.Ver_MinPatchDigits, flatten: false);
			else
				return a.Ver_Current.String(a.Ver_MinMajorDigits, a.Ver_MinMinorDigits, a.Ver_MinPatchDigits, flatten: false);
		}

		public void Version_SetLast (VersionNumber version)
		{
			Ver_LastMajor = version.Major;
			Ver_LastMinor = version.Minor;
			Ver_LastPatch = version.Patch;
		}
		#endregion

		#region Events
		public void Apply ()
		{
#if UNITY_EDITOR
			VersionNumber ver = new VersionNumber(Ver_Major, Ver_Minor, Ver_Patch);
			string versionStr = ver.String(Ver_MinMajorDigits, Ver_MinMinorDigits, Ver_MinPatchDigits, false);
			int versionInt = ver.Int(Ver_MinMajorDigits, Ver_MinMinorDigits, Ver_MinPatchDigits);
			PlayerSettings.bundleVersion = versionStr;
			PlayerSettings.iOS.buildNumber = versionStr;
			PlayerSettings.macOS.buildNumber = versionStr;
			PlayerSettings.Android.bundleVersionCode = versionInt;
			PlayerSettings.Lumin.versionCode = versionInt;
			PlayerSettings.PS4.appVersion = versionStr;
			PlayerSettings.Switch.displayVersion = versionStr;
			PlayerSettings.Switch.releaseVersion = versionStr;
			PlayerSettings.tvOS.buildNumber = versionStr;
			PlayerSettings.XboxOne.Version = versionStr;

			PlayerSettings.forceSingleInstance = Settings_SingleInstance;
			PlayerSettings.productName = Info_AppName;
			PlayerSettings.companyName = Info_CompanyName;

			UpdateAddonInfo();
#endif
		}

		public void UpdateAddonInfo ()
		{
			AddonName = AddonInformation.AddonName;
			AddonBy = AddonInformation.AddonBy;
			AddonVersion = AddonInformation.AddonVersion;
		}

#if UNITY_EDITOR
		public bool CheckProjectSettingsMatch ()
		{
			if (PlayerSettings.forceSingleInstance != Settings_SingleInstance
				|| PlayerSettings.productName != Info_AppName
				|| PlayerSettings.companyName != Info_CompanyName)
			{
				return false;
			}
			return true;
		}
#endif
		#endregion
	}

	#region Editor GUI
#if UNITY_EDITOR
	[CustomEditor(typeof(AppSettings))]
	[CanEditMultipleObjects]
	public class AppSettingsEditor : Editor
	{
		#region Vars
		SerializedProperty currentBuildNameType;
		SerializedProperty currentState;
		SerializedProperty currentAppName;
		SerializedProperty currentCompName;
		SerializedProperty Copyright;
		SerializedProperty currentSingleInstance;
		SerializedProperty currentEXEName;
		SerializedProperty BuildsPath;
		SerializedProperty MinMajorDigits;
		SerializedProperty MinMinorDigits;
		SerializedProperty MinPatchDigits;
		SerializedProperty Major;
		SerializedProperty Minor;
		SerializedProperty Patch;
		SerializedProperty LastBuildName;
		SerializedProperty LastState;
		SerializedProperty LastMajor;
		SerializedProperty LastMinor;
		SerializedProperty LastPatch;
		#endregion
		void OnEnable ()
		{
			currentBuildNameType = serializedObject.FindProperty("Build_NameFormat");
			currentState = serializedObject.FindProperty("Ver_AppDevStage");
			currentAppName = serializedObject.FindProperty("Info_AppName");
			currentCompName = serializedObject.FindProperty("Info_CompanyName");
			Copyright = serializedObject.FindProperty("Info_Copyright");
			currentSingleInstance = serializedObject.FindProperty("Settings_SingleInstance");
			currentEXEName = serializedObject.FindProperty("Build_EXEName");
			BuildsPath = serializedObject.FindProperty("Build_RootPath");
			MinMajorDigits = serializedObject.FindProperty("Ver_MinMajorDigits");
			MinMinorDigits = serializedObject.FindProperty("Ver_MinMinorDigits");
			MinPatchDigits = serializedObject.FindProperty("Ver_MinPatchDigits");
			Major = serializedObject.FindProperty("Ver_Major");
			Minor = serializedObject.FindProperty("Ver_Minor");
			Patch = serializedObject.FindProperty("Ver_Patch");
			LastMajor = serializedObject.FindProperty("Ver_LastMajor");
			LastMinor = serializedObject.FindProperty("Ver_LastMinor");
			LastPatch = serializedObject.FindProperty("Ver_LastPatch");
			LastState = serializedObject.FindProperty("Last_AppDevStage");
			LastBuildName = serializedObject.FindProperty("Last_BuildName");
		}

		public override void OnInspectorGUI ()
		{
			serializedObject.Update();

			EditorGUILayout.LabelField("Project Information", EditorStyles.boldLabel);
			EditorGUILayout.PropertyField(currentAppName);
			EditorGUILayout.PropertyField(currentCompName);
			EditorGUILayout.PropertyField(Copyright);

			EditorGUILayout.LabelField("Build Settings", EditorStyles.boldLabel);
			EditorGUILayout.PropertyField(currentSingleInstance);
			EditorGUILayout.PropertyField(currentEXEName);
			EditorGUILayout.PropertyField(BuildsPath);
			EditorGUILayout.PropertyField(currentBuildNameType);

			EditorGUILayout.LabelField("Versioning Settings", EditorStyles.boldLabel);
			EditorGUILayout.LabelField("Min. Digits");
			GUILayout.BeginHorizontal();
			EditorGUILayout.PropertyField(MinMajorDigits, GUIContent.none, GUILayout.Width(50f));
			EditorGUILayout.LabelField(".", GUILayout.Width(5f));
			EditorGUILayout.PropertyField(MinMinorDigits, GUIContent.none, GUILayout.Width(50f));
			EditorGUILayout.LabelField(".", GUILayout.Width(5f));
			EditorGUILayout.PropertyField(MinPatchDigits, GUIContent.none, GUILayout.Width(50f));
			GUILayout.EndHorizontal();

			EditorGUILayout.LabelField("Version", EditorStyles.boldLabel);
			GUILayout.BeginHorizontal();
			EditorGUILayout.PropertyField(currentState, new GUIContent(""), GUILayout.Width(70f));
			EditorGUILayout.PropertyField(Major, GUIContent.none, GUILayout.Width(50f));
			EditorGUILayout.LabelField(".", GUILayout.Width(5f));
			EditorGUILayout.PropertyField(Minor, GUIContent.none, GUILayout.Width(50f));
			EditorGUILayout.LabelField(".", GUILayout.Width(5f));
			EditorGUILayout.PropertyField(Patch, GUIContent.none, GUILayout.Width(50f));
			GUILayout.EndHorizontal();

			EditorGUILayout.LabelField("Last Build", EditorStyles.boldLabel);
			EditorGUILayout.PropertyField(LastBuildName);
			GUILayout.BeginHorizontal();
			EditorGUILayout.PropertyField(LastState, new GUIContent(""), GUILayout.Width(70f));
			EditorGUILayout.PropertyField(LastMajor, GUIContent.none, GUILayout.Width(50f));
			EditorGUILayout.LabelField(".", GUILayout.Width(5f));
			EditorGUILayout.PropertyField(LastMinor, GUIContent.none, GUILayout.Width(50f));
			EditorGUILayout.LabelField(".", GUILayout.Width(5f));
			EditorGUILayout.PropertyField(LastPatch, GUIContent.none, GUILayout.Width(50f));
			GUILayout.EndHorizontal();

			serializedObject.ApplyModifiedProperties();
		}
	}

	[ExecuteInEditMode]
	[CustomPropertyDrawer(typeof(AppSettings))]
	public class AppSettingsDrawerUIE : PropertyDrawer
	{
		public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
		{
			// Using BeginProperty / EndProperty on the parent property means that
			// prefab override logic works on the entire property.
			EditorGUI.BeginProperty(position, label, property);
			position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

			// Don't make child fields be indented
			var indent = EditorGUI.indentLevel;
			EditorGUI.indentLevel = 0;

			var normalRect = new Rect(position.x, position.y, position.width, position.height);
			var applyButton  = new Rect(position.x, position.y + 20f, position.width * 0.5f, position.height);
			var autoFindButton  = new Rect(position.x + (position.width * 0.5f), position.y + 20f, position.width * 0.5f, position.height);

			EditorGUI.PropertyField(normalRect, property, GUIContent.none);

			// Right Click Menu
			AppSettingsGUI.SORightClickMenu(property, normalRect);

			// Set indent back to what it was
			EditorGUI.indentLevel = indent;
			EditorGUI.EndProperty();
		}
	}

	[ExecuteInEditMode]
	public static class AppSettingsGUI
	{
		public static void SORightClickMenu (SerializedProperty property, Rect rect)
		{
			Event current = Event.current;
			if (rect.Contains(current.mousePosition) && current.type == EventType.ContextClick)
			{
				GenericMenu menu = new GenericMenu();
				AppSettings val = (AppSettings)property.objectReferenceValue;
				if (val != null)
				{
					if (!val.CheckProjectSettingsMatch())
					{
						menu.AddItem(new GUIContent("Apply Information to Project", "Applies the information from this AppSettings asset to the project."), false, val.Apply);
					}
				}
				bool sameAsCurrent = property.objectReferenceValue == AppSettings.ActiveAsset;
				menu.AddItem(new GUIContent("Get from Editor Utilites", "References the AppSettings asset used in Editor Utilities."), sameAsCurrent, SetProperty);
				
				void SetProperty ()
				{
					property.objectReferenceValue = AppSettings.ActiveAsset;
					property.serializedObject.ApplyModifiedProperties();
					property.serializedObject.Update();
				}

				menu.ShowAsContext();

				current.Use();
			}
		}

		public static void AutoRightClickMenu<T> (T obj, string propertyName, EventType current, bool FromSettings = false)
		{
			if (current == EventType.ContextClick)
			{
				Type type = obj.GetType();
				var field = type.GetField(propertyName);
				if (field == null)
				{
					return;
				}
				GenericMenu menu = new GenericMenu();
				AppSettings val = (AppSettings)field.GetValue(obj);
				if (val != null)
				{
					if (!val.CheckProjectSettingsMatch())
					{
						menu.AddItem(new GUIContent("Apply Information to Project", "Applies the information from this AppSettings asset to the project."), false, val.Apply);
					}
				}
				bool sameAsCurrent = val == AppSettings.ActiveAsset;

				if (!FromSettings)
				{
					menu.AddItem(new GUIContent("Get from Editor Utilites", "References the AppSettings asset used in Editor Utilities."), sameAsCurrent, SetValue);

					void SetValue ()
					{
						field.SetValue(obj, AppSettings.ActiveAsset);
					}
				}

				menu.ShowAsContext();
			}
		}
	}
#endif
	#endregion
}