﻿#region Credits
/* DynaZor's DynaTools
 * DynaTools by Shahar DynaZor Alon
 * Free Open Source software for the community <3
 * Feel free to contact me:
 * me@DynaZor.com
 * 
 * Originally, the project was a couple of tweaks to Vitaliy's Editor Utilities.
 * Since then, the whole code has been overhauled and 
 * Therefore I preserve the original credit and license :)
 */
/* Depcrecated; Vitaliy's Editor Utilities
 * Editor Utilities
 * The MIT License (MIT)
 * 
 * Copyright (c) 2015 Vitaliy Zasadnyy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
#endregion
#if UNITY_EDITOR
#region Using
using System;
using System.IO;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEditor.SceneManagement;
using UnityEditor.Build.Reporting;
using UnityEngine.SceneManagement;
using UnityEditor.AnimatedValues;
using DynaZor.DynaTools;
using DynaZor.DynaTools.DTScriptableObjects;
using DynaZor.DynaTools.DTScriptableObjects.DynaToolsSettings;
#endregion

namespace DynaZor.DynaTools
{
    public class DynaToolsWindow : EditorWindow
	{
		#region Variables
		#region Settings Assets
		private const string DT_Asset_Settings_File = "Settings_DynaToolsSettings.asset";
		private const string DT_Asset_Settings_Path = "Assets/Settings/";

		private const string DT_Asset_AppSettings_File = "Settings_AppSettings.asset";
		private const string DT_Asset_AppSettings_Path = "Assets/Settings/";

		private DynaToolsSettings settingsAsset;
		#endregion
		#region Info
		private string Unity_State = "";
		#endregion
		#region Window State
		private bool Window_State_InSettingsPanel;
		private float Window_State_Width = 0f;
		private float Window_State_MaxWidth => Window_State_Width - 20f;
		#endregion
		#region Utilities UI States
		private Vector2 _scrollViewPosition_Utilities;
		private Vector2 _scrollViewPosition_SceneShortcuts_Launch;
		private Vector2 _scrollViewPosition_SceneShortcuts_Load;
		#endregion
		#region Settings UI States
		private Vector2 _scrollViewPosition_Settings;
		#region From _settings
		private bool _Settings_AddonInfo_Open
		{
			get
			{
				if (settingsAsset != null)
					return settingsAsset._Settings_AddonInfo_Open;
				return true;
			}
			set
			{
				settingsAsset._Settings_AddonInfo_Open = value;
			}
		}
		private bool _Settings_AppSettings_Open
		{
			get
			{
				if (settingsAsset != null)
					return settingsAsset._Settings_AppSettings_Open;
				return true;
			}
			set
			{
				settingsAsset._Settings_AppSettings_Open = value;
			}
		}
		private bool _Settings_QuickBuild_Open
		{
			get
			{
				if (settingsAsset != null)
					return settingsAsset._Settings_QuickBuild_Open;
				return true;
			}
			set
			{
				settingsAsset._Settings_QuickBuild_Open = value;
			}
		}
		private bool _Settings_Preload_Open
		{
			get
			{
				if (settingsAsset != null)
					return settingsAsset._Settings_Preload_Open;
				return true;
			}
			set
			{
				settingsAsset._Settings_Preload_Open = value;
			}
		}
		private bool _Settings_MoreInfo_Open
		{
			get
			{
				if (settingsAsset != null)
					return settingsAsset._Settings_MoreInfo_Open;
				return true;
			}
			set
			{
				settingsAsset._Settings_MoreInfo_Open = value;
			}
		}
		#endregion
		private AnimBool _Settings_SceneShortcuts_Fold;
		private AnimBool _Settings_SceneShortcuts_Launch_Fold;
		private AnimBool _Settings_SceneShortcuts_Load_Fold;
		#endregion
		#region GUIStyles
		private GUIStyle Style_EditorStateLabel = new GUIStyle();
		private GUIStyle Style_CenterMe = new GUIStyle();
		private GUIStyle Style_SceneButton = new GUIStyle();
		private GUIStyle Style_TestBounds = new GUIStyle();
		private GUIStyle Style_MiniLabel_CenterMe
		{
			get
			{
				GUIStyle temp = new GUIStyle(EditorStyles.miniLabel);
				temp.alignment = TextAnchor.UpperCenter;
				temp.padding = new RectOffset(0, 0, 0, 0);
				return temp;
			}
		}
		private GUIStyle Style_FoldoutHeader (float subWidth = 0f)
		{
			GUIStyle temp = new GUIStyle(EditorStyles.foldoutHeader);
			temp.fixedWidth = Window_State_MaxWidth - subWidth;
			temp.stretchWidth = true;
			return temp;
		}
		private GUIStyle Style_Box
		{
			get
			{
				GUIStyle temp = new GUIStyle(GUI.skin.box);
				temp.overflow = new RectOffset();
				temp.margin = new RectOffset(1,0,0,0);
				temp.padding = new RectOffset(0,0,0,0);
				temp.contentOffset = Vector2.zero;
				return temp;
			}
		}
		private GUIStyle Style_MiniButton
		{
			get
			{
				GUIStyle temp = new GUIStyle(EditorStyles.miniButton);
				temp.overflow = new RectOffset();
				temp.margin = new RectOffset(0, 0, 0, 0);
				temp.padding = new RectOffset(0, 0, 0, 0);
				temp.contentOffset = Vector2.zero;
				return temp;
			}
		}
		private GUIStyle Style_NoPaddingNoMargins
		{
			get
			{
				GUIStyle temp = new GUIStyle();
				temp.overflow = new RectOffset();
				temp.margin = new RectOffset(0, 0, 0, 0);
				temp.padding = new RectOffset(0, 0, 0, 0);
				temp.contentOffset = Vector2.zero;
				return temp;
			}
		}
		private GUIStyle FoldoutHeader_def => Style_FoldoutHeader();

		private GUIStyle Style_TextField_Normal => EditorStyles.textField;

		private GUIStyle Style_TextField_Error
		{
			get
			{
				GUIStyle temp = new GUIStyle(EditorStyles.textField);
				temp.onActive.background = Texture2D.redTexture;
				temp.onFocused.background = Texture2D.redTexture;
				temp.onHover.background = Texture2D.redTexture;
				temp.onNormal.background = Texture2D.redTexture;
				return temp;
			}
		}
		#endregion
		#endregion
		#region Registeration
		[MenuItem("DynaZor/DynaTools", priority = 1)]
        public static void ShowWindow()
        {
			EditorWindow.GetWindow<DynaToolsWindow>(false, AddonInformation.AddonShortName, true);
		}
		#endregion
		#region Lifecycle
		private void OnEnable()
		{
			settingsAsset = LoadSettings();
			ApplyWindowProperties();
			Style_CenterMe.alignment = TextAnchor.UpperCenter;
			Style_CenterMe.padding = new RectOffset(0, 0, 0, 0);
			Style_SceneButton.alignment = TextAnchor.MiddleLeft;
			Style_SceneButton.padding = new RectOffset(0, 0, 0, 0);
			Style_SceneButton.stretchWidth = true;
			Style_SceneButton.margin = new RectOffset(0, 0, 0, 0);
			Texture2D bluePixel = new Texture2D(1,1);
			bluePixel.SetPixel(0, 0, Color.blue);
			bluePixel.Apply();
			Style_TestBounds.normal.background = bluePixel;

			UnityEngine.Events.UnityAction repaintEvent = new UnityEngine.Events.UnityAction(base.Repaint);

			_Settings_SceneShortcuts_Fold = new AnimBool(true);
			_Settings_SceneShortcuts_Fold.valueChanged.AddListener(repaintEvent);

			_Settings_SceneShortcuts_Launch_Fold = new AnimBool(false);
			_Settings_SceneShortcuts_Launch_Fold.valueChanged.AddListener(repaintEvent);

			_Settings_SceneShortcuts_Load_Fold = new AnimBool(false);
			_Settings_SceneShortcuts_Load_Fold.valueChanged.AddListener(repaintEvent);

			ApplyCurrentAppSettings();

			//DTEditorOnly.EditorHelpers.CheckVersionConsistency();
		}

		private void ApplyCurrentAppSettings ()
		{
			if (settingsAsset != null)
			if (AppSettings.ActiveAsset != settingsAsset.AppSettings)
			{
				AppSettings.ActiveAsset = settingsAsset.AppSettings;
			}
		}

		private void OnGUI()
		{
			Window_State_Width = EditorGUIUtility.currentViewWidth;
			GetCurrentState();
			if (settingsAsset == null)
			{
				settingsAsset = LoadSettings();
				this.Repaint();
			}
			ApplyWindowProperties();
			if (settingsAsset.AppSettings != null)
			{
				settingsAsset.AppSettings.CheckProjectSettingsMatch();
			}

			DrawToolbar();
			DrawWindowContent();
			if (settingsAsset == null)
				return;
			ApplyCurrentAppSettings();
		}

		private void Update()
        {
			string current = GetCurrentState();
			if (Unity_State != current)
			{
				Unity_State = current;
				Repaint();
			}
		}

		private void ApplyWindowProperties ()
		{
			if (settingsAsset != null)
				minSize = new Vector2(settingsAsset.Panel_Width_Min, settingsAsset.Panel_Height_Min);
		}
		#endregion
		#region UI Draw
		#region Toolbars
		private void DrawToolbar()
        {
            if (Window_State_InSettingsPanel)
            {
                DrawSettingsToolbar();
            }
            else
            {
                DrawUtilitiesToolbar();
            }
		}

		private void DrawUtilitiesToolbar ()
		{
			GUILayout.BeginHorizontal(EditorStyles.toolbar);
			GUILayout.FlexibleSpace();
			if (GUILayout.Button(new GUIContent("Recent Projects Editor", "Remove unneeded projects with one click"), EditorStyles.toolbarButton))
			{
				EditRecentProjectsWindow.ShowWindow();
			}

			if (GUILayout.Button("Settings", EditorStyles.toolbarButton))
			{
				Window_State_InSettingsPanel = true;
			}
			GUILayout.EndHorizontal();
		}

		private void DrawSettingsToolbar ()
		{
			GUILayout.BeginHorizontal(EditorStyles.toolbar);

			GUILayout.FlexibleSpace();

			var backUpColor = GUI.color;
			GUI.color = Color.green;
			if (GUILayout.Button("Done", EditorStyles.toolbarButton))
			{
				if (settingsAsset != null)
				{
					EditorUtility.SetDirty(settingsAsset);
					AssetDatabase.SaveAssets();
				}
				Window_State_InSettingsPanel = false;
			}
			GUI.color = backUpColor;

			GUILayout.EndHorizontal();
		}
		#endregion
		private void DrawWindowContent()
        {
            if (Window_State_InSettingsPanel)
            {
                DrawSettings_Panel();
            }
            else
            {
                DrawUtilities();
            }
        }
		#region Utilities
		private void DrawUtilities ()
		{
			using (var s = new EditorGUILayout.ScrollViewScope(_scrollViewPosition_Utilities, GUILayout.MaxWidth(Window_State_Width - 2f)))
			{
				_scrollViewPosition_Utilities = s.scrollPosition;
				bool appSettingsPresent = settingsAsset != null && settingsAsset.AppSettings != null;
				if (appSettingsPresent)
				{
					DrawAppInfo();

					if (settingsAsset.showQuickShortcuts | settingsAsset.showEditorState)
					{
						using (new EditorGUILayout.HorizontalScope(GUILayout.Height(40f), GUILayout.Width(Window_State_Width - 20f)))
						{
							GUILayout.FlexibleSpace();
							if (settingsAsset.showQuickShortcuts)
							{
								GUILayout.Space(2f);
								DrawQuickShortcuts();
								GUILayout.Space(2f);
							}
							if (settingsAsset.showEditorState)
							{
								DrawEditorState();
							}
							GUILayout.FlexibleSpace();
						}
					}
					if (settingsAsset.showVersion)
					{
						DrawVersionInformation();
					}

					if (settingsAsset.showQuickBuild)
					{
						DrawQuickBuild();
					}

					if (settingsAsset.showSceneShortcuts)
					{
						DrawSceneShortcuts();
					}
				}
				else
				{
					EditorGUILayout.HelpBox("No AppSettings Asset Loaded!\nDynaTools will partially function.", MessageType.Warning);
					DrawAppSettings_Chooser_NoAssetLoaded();
					DrawEditorState();

					using (new EditorGUILayout.HorizontalScope(GUILayout.Height(40f), GUILayout.Width(settingsAsset.Button_Quick_Play_Width + (settingsAsset.LoadPreloadScene ? settingsAsset.Button_Quick_Preload_Width : 0) - 30f)))
					{
						GUILayout.FlexibleSpace();
						bool playing = EditorApplication.isPlaying;
						if (GUILayout.Button(new GUIContent(playing?"Stop" :  "Play", "Start\\Stop Play Mode"),  GUILayout.Height(40f), GUILayout.Width(55f)))
						{
							EditorApplication.isPlaying = !playing;
						}
						GUILayout.FlexibleSpace();
					}
				}
			}
		}

		private void DrawAppInfo ()
		{
			using (new EditorGUILayout.HorizontalScope(GUILayout.MaxWidth(Window_State_Width - 20f)))
			{
				GUILayout.Space(2.5f);
				GUILayout.FlexibleSpace();
				using (new EditorGUILayout.HorizontalScope(GUILayout.MinWidth(settingsAsset.Info_Field_Width_Min * 2 + 20f), GUILayout.MaxWidth(settingsAsset.Info_Field_Width_Max * 2 + 20f)))
				{
					using (var cc = new EditorGUI.ChangeCheckScope())
					{
						GUILayout.Space(2.5f);

						UI_HightlightBackground_Start(CheckFieldEmpty(settingsAsset.AppSettings.Info_AppName), Color.red, Color.red);
						string newAppName = EditorGUILayout.TextField(settingsAsset.AppSettings.Info_AppName, GUILayout.ExpandWidth(true), GUILayout.MinWidth(settingsAsset.Info_Field_Width_Min), GUILayout.MaxWidth(settingsAsset.Info_Field_Width_Max));
						UI_HightlightBackground_End();

						EditorGUILayout.LabelField("by", GUILayout.Width(17.5f));
						UI_HightlightBackground_Start(CheckFieldEmpty(settingsAsset.AppSettings.Info_CompanyName), Color.red, Color.red);
						string newCompName = EditorGUILayout.TextField(settingsAsset.AppSettings.Info_CompanyName, GUILayout.ExpandWidth(true), GUILayout.MinWidth(settingsAsset.Info_Field_Width_Min), GUILayout.MaxWidth(settingsAsset.Info_Field_Width_Max));
						UI_HightlightBackground_End();

						if (cc.changed)
						{
							settingsAsset.AppSettings.Info_AppName = newAppName;
							settingsAsset.AppSettings.Info_CompanyName = newCompName;
						}
					}
				}
				GUILayout.FlexibleSpace();
			}
			DrawAppSettingsWarnningErrors(true);
		}

		private void DrawEditorState ()
		{
			using (new EditorGUILayout.HorizontalScope(GUILayout.Height(40f), GUILayout.Width(90f)))
			{
				GUILayout.FlexibleSpace();
				EditorGUILayout.LabelField(Unity_State, Style_EditorStateLabel, GUILayout.Height(40f), GUILayout.Width(90f));
				GUILayout.FlexibleSpace();
			}
		}

		private void DrawQuickShortcuts ()
		{
			using (new EditorGUILayout.HorizontalScope(GUILayout.Height(40f), GUILayout.Width(settingsAsset.Button_Quick_Play_Width + (settingsAsset.LoadPreloadScene? settingsAsset.Button_Quick_Preload_Width : 0) - 30f)))
			{
				GUILayout.FlexibleSpace();
				bool playing = EditorApplication.isPlaying;
				if (GUILayout.Button(new GUIContent(playing ? "Pause" : "Play", settingsAsset.LoadPreloadScene? "Preload and then s" : "S" + "tart\\Stop Play Mode"), 
					GUILayout.Height(40f), GUILayout.Width(settingsAsset.Button_Quick_Play_Width)))
				{
					int type = Event.current.button;
					if (type == 0)
					{
						if (!EditorApplication.isPlaying)
						{
							if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
							{
								//!! 9EU: Implement Play Scene setting and uncomment below
								//CheckAndLoadPlayScene();
								CheckAndAddLoadPreloadScene();
								EditorApplication.isPlaying = true;
							}
						}
					}
					else if (type == 1)
					{
						//!! 9EU: Implement Last Loaded Scenes memory restore
						EditorApplication.isPlaying = false;
					}
				}

				if (settingsAsset.LoadPreloadScene)
				{
					if (GUILayout.Button(new GUIContent("Preload", "Additive Load the Preload Scene\nWill not start Play Mode\nRight Click: Unload Preload Scene"), GUILayout.Height(40f), GUILayout.Width(settingsAsset.Button_Quick_Preload_Width)))
					{
						if (!EditorApplication.isPlaying)
						{
							int type = Event.current.button;

							if (type == 0)
							{
								CheckAndAddLoadPreloadScene();
							}
							else if (type == 1)
							{
								CheckAndUnloadPreloadScene();
							}
						}
					}
				}
				GUILayout.FlexibleSpace();
			}
		}

		private void CheckAndLoadPlayScene ()
		{
			CheckAndLoadScene(settingsAsset._playScene);
		}

		private void DrawVersionInformation ()
		{
			using (new EditorGUILayout.HorizontalScope(GUILayout.MaxWidth(Window_State_Width - 5f)))
			{
				using (var cc = new EditorGUI.ChangeCheckScope())
				{
					GUILayout.FlexibleSpace();
					var newState = (AppSettings.DevStage) EditorGUILayout.EnumPopup(settingsAsset.AppSettings.Ver_AppDevStage, GUILayout.Width(70f));
					var newMajor = EditorGUILayout.IntField(settingsAsset.AppSettings.Ver_Major, GUILayout.Width(settingsAsset.VersionField_Width));
					EditorGUILayout.LabelField(".", GUILayout.Width(5f));
					var newMinor = EditorGUILayout.IntField(settingsAsset.AppSettings.Ver_Minor, GUILayout.Width(settingsAsset.VersionField_Width));
					EditorGUILayout.LabelField(".", GUILayout.Width(5f));
					var newPatch = EditorGUILayout.IntField(settingsAsset.AppSettings.Ver_Patch, GUILayout.Width(settingsAsset.VersionField_Width));
					GUILayout.FlexibleSpace();

					if (cc.changed)
					{
						settingsAsset.AppSettings.Ver_AppDevStage = newState;
						settingsAsset.AppSettings.Ver_Major = newMajor;
						settingsAsset.AppSettings.Ver_Minor = newMinor;
						settingsAsset.AppSettings.Ver_Patch = newPatch;
					}
				}
			}
		}

		private void DrawQuickBuild ()
		{
			using (new EditorGUILayout.HorizontalScope(GUILayout.MaxWidth(Window_State_Width - 5f)))
			{
				using (var cc = new EditorGUI.ChangeCheckScope())
				{
					GUILayout.FlexibleSpace();
					EditorGUILayout.LabelField("Name Format:", GUILayout.Width(80f));
					var newSett = (AppSettings.BuildNameFormat)EditorGUILayout.EnumPopup(settingsAsset.AppSettings.Build_NameFormat, GUILayout.Width(100f));
					GUILayout.FlexibleSpace();

					if (cc.changed)
					{
						settingsAsset.AppSettings.Build_NameFormat = newSett;
					}
				}
			}

			using (new EditorGUILayout.HorizontalScope(GUILayout.MinWidth(110f), GUILayout.MaxWidth(Window_State_Width - 5f)))
			{
				GUILayout.FlexibleSpace();

				if (GUILayout.Button(new GUIContent("Build", "Quick Build to:\n" + (GetBuildPath(false, out _, true) ?? "Error") + "\nRight Click: Build To... (Browse)"), GUILayout.Height(30f), GUILayout.Width(50f)))
				{
					int type = Event.current.button;
					if (type == 0)
					{
						IssueBuild(false);
					}
					else if (type == 1)
					{
						IssueBuild(true);
					}
				}

				if (settingsAsset.showQuickBuildSettings | settingsAsset.showQuickBuildTarget)
				{
					using (new EditorGUILayout.VerticalScope(GUILayout.Height(35f), GUILayout.MinWidth(60f), GUILayout.MaxWidth(150f)))
					{
						GUILayout.FlexibleSpace();
						if (settingsAsset.showQuickBuildSettings)
						{
							using (var cc = new EditorGUI.ChangeCheckScope())
							{
								var newSett = (DynaToolsSettings.BuildSettingsType)EditorGUILayout.EnumFlagsField(new GUIContent("", "Quick Build Options"), settingsAsset.BuildSettings, GUILayout.Height(15f), GUILayout.MinWidth(60f), GUILayout.MaxWidth(150f));
								if (cc.changed)
								{
									settingsAsset.BuildSettings = newSett;
								}
							}
						}
						if (settingsAsset.showQuickBuildTarget)
						{
							var newSett = (BuildTarget)EditorGUILayout.EnumPopup(new GUIContent("", "Quick Build Target"), settingsAsset.selectedBuildTarget, null, false, GUILayout.Height(15f), GUILayout.MinWidth(60f), GUILayout.MaxWidth(150f));

							using (var cc = new EditorGUI.ChangeCheckScope())
							{
								if (cc.changed)
								{
									settingsAsset.selectedBuildTarget = newSett;
								}
							}
						}
						GUILayout.FlexibleSpace();
					}
				}

				GUILayout.FlexibleSpace();
			}

			settingsAsset.AppSettings.Apply();

			if (settingsAsset.showLastBuild)
			{
				DrawLastBuild();
			}

		}

		private void IssueBuild (bool BrowseFirst)
		{
			settingsAsset.AppSettings.Apply();
			// Starting BuildPlayerOptions
			string buildPath = GetBuildPath(BrowseFirst, out string folderName);
			if (string.IsNullOrEmpty(buildPath) | string.IsNullOrWhiteSpace(buildPath))
			{
				return;
			}
			BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
			buildPlayerOptions.locationPathName = buildPath;

			List<EditorBuildSettingsScene> editorBuildSettingsScenes = new List<EditorBuildSettingsScene>();
			editorBuildSettingsScenes.AddRange(EditorBuildSettings.scenes);
			List<string> ScenePaths = new List<string>();
			foreach (EditorBuildSettingsScene scene in editorBuildSettingsScenes)
				ScenePaths.Add(scene.path);

			buildPlayerOptions.target = settingsAsset.selectedBuildTarget;

			buildPlayerOptions.scenes = ScenePaths.ToArray();

			buildPlayerOptions.options = settingsAsset.selectedBuildOptions;

			bool isFinal = settingsAsset.AppSettings.Ver_AppDevStage == AppSettings.DevStage.Final;
			bool alwaysDevOn = settingsAsset.BuildSettings.HasFlag(DynaToolsSettings.BuildSettingsType.AlwaysDev);
			bool autoDevOn = settingsAsset.BuildSettings.HasFlag(DynaToolsSettings.BuildSettingsType.AutoDev);
			if (alwaysDevOn || (autoDevOn && isFinal))
			{
				buildPlayerOptions.options |= BuildOptions.Development;
			}

			if (buildPlayerOptions.options.HasFlag(BuildOptions.Development))
			{
				Helpers.DebugLog("Starting Dev Build");
			}
			else
			{
				Helpers.DebugLog("Starting Non-Dev Build");
			}

			settingsAsset.AppSettings.Last_AppDevStage = settingsAsset.AppSettings.Ver_AppDevStage;
			settingsAsset.AppSettings.Version_SetLast(settingsAsset.AppSettings.Ver_Current);
			settingsAsset.AppSettings.Last_BuildName = folderName;
			settingsAsset.AppSettings.Apply();

			// Start build
			BuildResult buildResult = BuildPipeline.BuildPlayer(buildPlayerOptions).summary.result;
			bool buildSucess = buildResult == BuildResult.Succeeded;
			if (buildSucess && settingsAsset.BuildSettings.HasFlag(DynaToolsSettings.BuildSettingsType.OpenFolder))
			{
				UnityEditor.EditorUtility.RevealInFinder(buildPlayerOptions.locationPathName);
			}
			Helpers.DebugLog("Build " + buildResult.ToString());
		}

		private void DrawLastBuild ()
		{
			using (new EditorGUILayout.HorizontalScope(GUILayout.MaxWidth(Window_State_Width - 5f)))
			{
				EditorGUILayout.LabelField("Last Build:\n" + settingsAsset.AppSettings.Last_AppDevStage.ToString()
								+ " v" + AppSettings.VersionToString(settingsAsset.AppSettings, true)
								+ " (\"" + settingsAsset.AppSettings.Last_BuildName + "\")", Style_MiniLabel_CenterMe, GUILayout.Height(30f));
			}
		}

		private void DrawSceneShortcuts ()
		{
			float maxHeight = 25f * Mathf.Clamp((settingsAsset.launchSceneShortcuts.Count > settingsAsset.loadSceneShortcuts.Count? settingsAsset.launchSceneShortcuts.Count : settingsAsset.loadSceneShortcuts.Count) + 1,2,10);
			float minHeight = 25f * Mathf.Clamp((settingsAsset.launchSceneShortcuts.Count > settingsAsset.loadSceneShortcuts.Count? settingsAsset.launchSceneShortcuts.Count : settingsAsset.loadSceneShortcuts.Count) + 1,1,3);
			using (var v1 = new EditorGUILayout.VerticalScope(GUILayout.MinHeight(minHeight), GUILayout.MaxHeight(maxHeight), GUILayout.ExpandHeight(true)))
			{
				using (var h1 = new EditorGUILayout.HorizontalScope(GUILayout.MinWidth(settingsAsset.Button_LaunchScene_Width_Min + settingsAsset.Button_LoadScene_Width_Min + settingsAsset.Button_LoadSceneAdditive_Width_Min + 5f), GUILayout.MaxWidth(Window_State_Width - 15f)))
				{
					GUILayout.FlexibleSpace();
					using (var h2 = new EditorGUILayout.VerticalScope(GUILayout.MinWidth(settingsAsset.Button_LaunchScene_Width_Min - 15f), GUILayout.MaxWidth(Window_State_Width - 20f)))
					{
						EditorGUILayout.LabelField("Launch Scene:", EditorStyles.boldLabel, GUILayout.Width(95f), GUILayout.Height(15f));
						if (settingsAsset.launchSceneShortcuts.Count > 0)
						{
							using (var s = new EditorGUILayout.ScrollViewScope(_scrollViewPosition_SceneShortcuts_Launch, GUILayout.MinHeight(minHeight), GUILayout.MaxHeight(maxHeight), GUILayout.MinWidth(settingsAsset.Button_LaunchScene_Width_Min), GUILayout.MaxWidth(Window_State_Width - 5f)))
							{
								_scrollViewPosition_SceneShortcuts_Launch = s.scrollPosition;
								for (int i = 0; i < settingsAsset.launchSceneShortcuts.Count; i++)
								{
									using (var h3 = new EditorGUILayout.HorizontalScope(GUILayout.MinWidth(settingsAsset.Button_LaunchScene_Width_Min - 15f), GUILayout.MaxWidth(Window_State_Width - 20f)))
									{
										Scene currentScene = SceneManager.GetSceneByPath(settingsAsset.launchSceneShortcuts[i]);
										string sceneName = settingsAsset.launchSceneShortcuts[i];
										string[] sceneNameParts = sceneName.Split('/');
										sceneName = sceneNameParts[sceneNameParts.Length - 1].Replace(".unity", "");
										if (GUILayout.Button(sceneName + (currentScene.buildIndex == -1 ? "" : " (" + currentScene.buildIndex + ")"),
											GUILayout.MaxWidth(300f),
											GUILayout.MinWidth(settingsAsset.Button_LaunchScene_Width_Min)))
										{
											if (!EditorApplication.isPlaying)
											{
												if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
												{
													EditorSceneManager.OpenScene(settingsAsset.launchSceneShortcuts[i], OpenSceneMode.Single);
													CheckAndAddLoadPreloadScene();
													EditorApplication.isPlaying = true;
												}
											}
										}
									}
								}
							}
						}
						else
						{
							DrawLabel_NothingToShow();
						}
					}

					GUILayout.FlexibleSpace();

					using (var h2 = new EditorGUILayout.VerticalScope(GUILayout.MinWidth(settingsAsset.Button_LoadScene_Width_Min + settingsAsset.Button_LoadSceneAdditive_Width_Min - 15f), GUILayout.MaxWidth(Window_State_Width - 20f)))
					{
						EditorGUILayout.LabelField("Load Scene:", EditorStyles.boldLabel, GUILayout.Width(80f), GUILayout.Height(15f));
						if (settingsAsset.loadSceneShortcuts.Count > 0)
						{
							using (var s = new EditorGUILayout.ScrollViewScope(_scrollViewPosition_SceneShortcuts_Load, GUILayout.MinHeight(minHeight), GUILayout.MaxHeight(maxHeight), GUILayout.MinWidth(settingsAsset.Button_LoadScene_Width_Min + settingsAsset.Button_LoadSceneAdditive_Width_Min - 20f), GUILayout.MaxWidth(Window_State_Width - 20f)))
							{
								_scrollViewPosition_SceneShortcuts_Load = s.scrollPosition;
								for (int i = 0; i < settingsAsset.loadSceneShortcuts.Count; i++)
								{
									using (var h3 = new EditorGUILayout.HorizontalScope(GUILayout.MinWidth(settingsAsset.Button_LoadScene_Width_Min + settingsAsset.Button_LoadSceneAdditive_Width_Min - 20f), GUILayout.MaxWidth(Window_State_Width - 20f)))
									{
										Scene currentScene = EditorSceneManager.GetSceneByPath(settingsAsset.loadSceneShortcuts[i]);
										string sceneName = settingsAsset.loadSceneShortcuts[i];
										string[] sceneNameParts = sceneName.Split('/');
										sceneName = sceneNameParts[sceneNameParts.Length - 1].Replace(".unity", "");
										if (GUILayout.Button(sceneName + (currentScene.buildIndex == -1 ? "" : " (" + currentScene.buildIndex + ")"),
											GUILayout.MaxWidth(300f),
											GUILayout.MinWidth(settingsAsset.Button_LoadScene_Width_Min)))
										{
											if (!EditorApplication.isPlaying)
											{
												if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
												{
													EditorSceneManager.OpenScene(settingsAsset.loadSceneShortcuts[i], OpenSceneMode.Single);
												}
											}
										}

										if (GUILayout.Button(new GUIContent("+", "Additive Load Scene\nRight Click: Unload Scene"),
											GUILayout.MinWidth(settingsAsset.Button_LoadSceneAdditive_Width_Min),
											GUILayout.MaxWidth(settingsAsset.Button_LoadSceneAdditive_Width_Max)))
										{
											if (!EditorApplication.isPlaying | settingsAsset.additiveLoadSceneWhilePlaying)
											{ 
												int type = Event.current.button;
												if (type == 0)
												{
													CheckAndAddLoadScene(settingsAsset.loadSceneShortcuts[i]);
												}
												else if (type == 1)
												{
													if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
													{
														CheckAndUnloadScene(settingsAsset.loadSceneShortcuts[i]);
													}
												}
											}
										}
									}
								}
							}
						}
						else
						{
							DrawLabel_NothingToShow();
						}
					}

					GUILayout.FlexibleSpace();
				}
			}
		}

		public static void DrawLabel_NothingToShow ()
		{
			EditorGUILayout.LabelField("Nothing to show", GUILayout.Width(100f));
		}
		#endregion
		#region Settings
		private void DrawSettings_Panel ()
		{
			if (settingsAsset == null)
			{
				LoadSettings();
				this.Repaint();
			}
			else if (settingsAsset.AppSettings == null)
			{
				DrawAppSettings_Chooser_NoAssetLoaded();
			}
			else
			{
				using (var s = new EditorGUILayout.ScrollViewScope(_scrollViewPosition_Settings, false, false, GUI.skin.horizontalScrollbar, GUI.skin.verticalScrollbar, GUI.skin.scrollView))
				{
					_scrollViewPosition_Settings = s.scrollPosition;
					using (new EditorGUILayout.VerticalScope(GUILayout.MaxWidth(Window_State_MaxWidth)))
					{
						DrawAddonInfo();

						DrawSettings_AppSettings();
						EditorGUILayout.Space();

						using (var cc = new EditorGUI.ChangeCheckScope())
						{
							var newEdState = GUILayout.Toggle(settingsAsset.showEditorState, "Show Editor State");
							var newQuick = GUILayout.Toggle(settingsAsset.showQuickShortcuts, "Show Play Button");
							if (cc.changed)
							{
								settingsAsset.showEditorState = newEdState;
								settingsAsset.showQuickShortcuts = newQuick;
							}
						}

						EditorGUILayout.Space();
						EditorGUI.BeginDisabledGroup(!settingsAsset.showQuickShortcuts);
						DrawSettings_Toggles();
						DrawSettings_Preload();

						EditorGUI.EndDisabledGroup();

						DrawSettingsSceneShortcuts();
						DrawSettings_QuickBuild();

						EditorGUILayout.Space();
						DrawSettings_Advanced();
						EditorGUILayout.Space();
					}
				}
			}
		}

		private void DrawAppSettings_Chooser_NoAssetLoaded ()
		{
			using (new EditorGUILayout.VerticalScope(GUILayout.MaxWidth(Window_State_MaxWidth)))
			{
				DrawAddonInfo();

				GUILayout.Space(2f);

				EditorGUILayout.LabelField("App Settings Asset");
				int id = GUIUtility.GetControlID(FocusType.Keyboard);

				using (var cc = new EditorGUI.ChangeCheckScope())
				{
					var a1 = EditorGUILayout.ObjectField(settingsAsset.AppSettings, typeof(AppSettings), false) as AppSettings;

					if (cc.changed)
					{
						settingsAsset.AppSettings = a1;
					}
				}

				if (GUILayout.Button("Create"))
				{
					AppSettings asset = AssetDatabase.LoadAssetAtPath<AppSettings>(DT_Asset_AppSettings_Path + DT_Asset_AppSettings_File);
					
					if (asset == null)
					{
						asset = AssetDatabase.LoadAssetAtPath<AppSettings>(CreateAsset<AppSettings>(DT_Asset_AppSettings_Path, DT_Asset_AppSettings_File));
					}

					settingsAsset.AppSettings = asset;
					UpdateInformation();
					this.Repaint();
				}

				AppSettingsGUI.AutoRightClickMenu(settingsAsset, "AppSettings", Event.current.GetTypeForControl(id), true);
			}
		}

		private void DrawAddonInfo ()
		{
			bool open = true;
			if (settingsAsset != null) 
			{ 
				_Settings_AddonInfo_Open = EditorGUILayout.BeginFoldoutHeaderGroup(_Settings_AddonInfo_Open, AddonInformation.AddonName, FoldoutHeader_def);
				open = _Settings_AddonInfo_Open;
			}

			if (settingsAsset == null || open)
			{
				using (new EditorGUILayout.VerticalScope(Style_Box))
				{
					EditorGUILayout.LabelField("Version" + AddonInformation.AddonVersion.Replace('v', ' '));
					EditorGUILayout.LabelField("by " + AddonInformation.AddonBy);

					if (settingsAsset != null)
					{
						_Settings_MoreInfo_Open = EditorGUILayout.ToggleLeft(new GUIContent("Show Advanced Information"), _Settings_MoreInfo_Open, EditorStyles.miniLabel);
					}

					if (settingsAsset == null || _Settings_MoreInfo_Open)
					{
						EditorGUILayout.LabelField("Unity " + Application.unityVersion, EditorStyles.miniLabel);
						EditorGUILayout.LabelField("C# " + System.Environment.Version.ToString(), EditorStyles.miniLabel);
						EditorGUILayout.LabelField("Detected System Language: " + Application.systemLanguage.ToString(), EditorStyles.miniLabel);
					}
				}
			}
			EditorGUILayout.EndFoldoutHeaderGroup();
		}

		private void DrawSettings_Preload ()
		{
			EditorGUILayout.Space();

			using (var cc = new EditorGUI.ChangeCheckScope())
			{
				var newSett = EditorGUILayout.BeginFoldoutHeaderGroup(_Settings_Preload_Open, "Additive Preload Scene " + (settingsAsset.LoadPreloadScene ? "(On)" : "(Off)"), FoldoutHeader_def);
				if (cc.changed)
				{
					_Settings_Preload_Open = newSett;
				}
			}
			
			if (_Settings_Preload_Open)
			{
				using (var h = new EditorGUILayout.VerticalScope(Style_Box))
				{
					using (var cc = new EditorGUI.ChangeCheckScope())
					{
						var preload = GUILayout.Toggle(settingsAsset.LoadPreloadScene, "Enable");
						EditorGUI.BeginDisabledGroup(!settingsAsset.LoadPreloadScene);
						var scene = DrawScenePathField(settingsAsset._preloadScene, false);
						EditorGUI.EndDisabledGroup();
						if (cc.changed)
						{
							settingsAsset.LoadPreloadScene = preload;
							settingsAsset._preloadScene = scene;
						}
					}
				}
			}
			EditorGUILayout.EndFoldoutHeaderGroup();
		}

		private void DrawSettings_QuickBuild ()
		{
			if (settingsAsset.AppSettings != null)
			{
				GUILayout.Space(5f);

				using (var cc = new EditorGUI.ChangeCheckScope())
				{
					var newSett = EditorGUILayout.BeginFoldoutHeaderGroup(_Settings_QuickBuild_Open, "Quick Build Settings " + (settingsAsset.showQuickBuild ? "(On)" : " (Off)"), FoldoutHeader_def);
					if (cc.changed)
					{
						_Settings_QuickBuild_Open = newSett;
					}
				}
				
				EditorGUI.BeginDisabledGroup(!settingsAsset.showQuickBuild);
				if (_Settings_QuickBuild_Open)
				{
					using (new EditorGUILayout.VerticalScope(Style_Box, GUILayout.MinWidth(120f), GUILayout.MaxWidth(Window_State_MaxWidth)))
					{
						using (var cc = new EditorGUI.ChangeCheckScope())
						{
							float prevWidth = EditorGUIUtility.labelWidth;
							EditorGUIUtility.labelWidth = 120f;

							var options = (BuildOptions) EditorGUILayout.EnumFlagsField("Persistent Options", settingsAsset.selectedBuildOptions, GUILayout.MinWidth(80f), GUILayout.MaxWidth(Window_State_MaxWidth));
							var settings = (DynaToolsSettings.BuildSettingsType) EditorGUILayout.EnumFlagsField("Quick Settings", settingsAsset.BuildSettings, GUILayout.MinWidth(80f), GUILayout.MaxWidth(Window_State_MaxWidth));
							var showQuick = GUILayout.Toggle(settingsAsset.showQuickBuildSettings, "Show Quick Settings");

							GUILayout.Space(5f);

							var selTarget = (BuildTarget) EditorGUILayout.EnumPopup(new GUIContent("Selected Target"), settingsAsset.selectedBuildTarget, null, false, GUILayout.MinWidth(80f), GUILayout.MaxWidth(Window_State_MaxWidth));
							var showTarget = GUILayout.Toggle(settingsAsset.showQuickBuildTarget, "Show Quick Target");

							EditorGUIUtility.labelWidth = prevWidth;
							if (cc.changed)
							{
								settingsAsset.selectedBuildOptions = options;
								settingsAsset.BuildSettings = settings;
								settingsAsset.showQuickBuildSettings = showQuick;
								settingsAsset.selectedBuildTarget = selTarget;
								settingsAsset.showQuickBuildTarget = showTarget;
							}
						}
						
					}
				}
				EditorGUI.EndDisabledGroup();
				EditorGUILayout.EndFoldoutHeaderGroup();
			}
		}

		private void DrawSettings_Toggles ()
		{
			using (var cc = new EditorGUI.ChangeCheckScope())
			{
				EditorGUI.BeginDisabledGroup(settingsAsset.AppSettings == null);
				var showInfo = GUILayout.Toggle(settingsAsset.showInfoSettings, "Show Quick Info");
				var showVer = GUILayout.Toggle(settingsAsset.showVersion, "Show Version Control");
				var showBuild = GUILayout.Toggle(settingsAsset.showQuickBuild, "Show Quick Build");

				EditorGUI.BeginDisabledGroup(!settingsAsset.showQuickBuild);
				var showLast = GUILayout.Toggle(settingsAsset.showLastBuild, "Show Last Build Information");
				EditorGUI.EndDisabledGroup();

				EditorGUI.EndDisabledGroup();

				var showShortcuts = GUILayout.Toggle(settingsAsset.showSceneShortcuts, "Show Scene Schortcuts");
				if (cc.changed)
				{
					settingsAsset.showInfoSettings = showInfo;
					settingsAsset.showVersion = showVer;
					settingsAsset.showQuickBuild = showBuild;
					settingsAsset.showLastBuild = showLast;
					settingsAsset.showSceneShortcuts = showShortcuts;
				}
			}
		}

		private void DrawSettings_Advanced ()
		{
			using (var cc = new EditorGUI.ChangeCheckScope())
			{
				var showAdv = EditorGUILayout.BeginFoldoutHeaderGroup(settingsAsset.showAdvancedSettings, "Advanced Settings", FoldoutHeader_def);
				if (cc.changed)
				{
					settingsAsset.showAdvancedSettings = showAdv;
				}
			}
			

			if (settingsAsset.showAdvancedSettings)
			{
				using (var cc = new EditorGUI.ChangeCheckScope())
				{
					float prevWidth = EditorGUIUtility.labelWidth;
					EditorGUIUtility.labelWidth = settingsAsset.Settings_Advanced_LabelWidth;
					var a1 = EditorGUILayout.Toggle("additiveLoadSceneWhilePlaying", settingsAsset.additiveLoadSceneWhilePlaying, GUILayout.ExpandWidth(false));
					var a2 = EditorGUILayout.FloatField("Panel_Height_Min", settingsAsset.Panel_Height_Min, GUILayout.ExpandWidth(false));
					var a3 = EditorGUILayout.FloatField("Panel_Width_Min", settingsAsset.Panel_Width_Min, GUILayout.ExpandWidth(false));
					var a4 = EditorGUILayout.FloatField("Settings_Advanced_LabelWidth", settingsAsset.Settings_Advanced_LabelWidth, GUILayout.ExpandWidth(false));
					var a5 = EditorGUILayout.FloatField("Button_LaunchScene_Width_Min", settingsAsset.Button_LaunchScene_Width_Min, GUILayout.ExpandWidth(false));
					var a6 = EditorGUILayout.FloatField("SceneShortcuts_Height_Min", settingsAsset.SceneShortcuts_Height_Min, GUILayout.ExpandWidth(false));
					var a7 = EditorGUILayout.FloatField("Button_LoadScene_Width_Min", settingsAsset.Button_LoadScene_Width_Min, GUILayout.ExpandWidth(false));
					var a8 = EditorGUILayout.FloatField("Button_LoadSceneAdditive_Width_Min", settingsAsset.Button_LoadSceneAdditive_Width_Min, GUILayout.ExpandWidth(false));
					var a9 = EditorGUILayout.FloatField("Button_LoadSceneAdditive_Width_Max", settingsAsset.Button_LoadSceneAdditive_Width_Max, GUILayout.ExpandWidth(false));
					var b1 = EditorGUILayout.FloatField("SceneLists_Width_Max", settingsAsset.SceneLists_Width_Max, GUILayout.ExpandWidth(false));
					var b2 = EditorGUILayout.FloatField("VersionField_Width", settingsAsset.VersionField_Width, GUILayout.ExpandWidth(false));
					var b3 = EditorGUILayout.FloatField("Info_Field_Width_Min", settingsAsset.Info_Field_Width_Min, GUILayout.ExpandWidth(false));
					var b4 = EditorGUILayout.FloatField("Info_Field_Width_Max", settingsAsset.Info_Field_Width_Max, GUILayout.ExpandWidth(false));
					var b5 = EditorGUILayout.FloatField("Settings_SceneLists_SpaceAfterItem", settingsAsset.Settings_SceneLists_SpaceAfterItem, GUILayout.ExpandWidth(false));
					EditorGUIUtility.labelWidth = prevWidth;
					if (cc.changed)
					{
						settingsAsset.additiveLoadSceneWhilePlaying = a1;
						settingsAsset.Panel_Height_Min = a2;
						settingsAsset.Panel_Width_Min = a3;
						settingsAsset.Settings_Advanced_LabelWidth = a4;
						settingsAsset.Button_LaunchScene_Width_Min = a5;
						settingsAsset.SceneShortcuts_Height_Min = a6;
						settingsAsset.Button_LoadScene_Width_Min = a7;
						settingsAsset.Button_LoadSceneAdditive_Width_Min = a8;
						settingsAsset.Button_LoadSceneAdditive_Width_Max = a9;
						settingsAsset.SceneLists_Width_Max = b1;
						settingsAsset.VersionField_Width = b2;
						settingsAsset.Info_Field_Width_Min = b3;
						settingsAsset.Info_Field_Width_Max = b4;
						settingsAsset.Settings_SceneLists_SpaceAfterItem = b5;
					}
				}
			}
			EditorGUILayout.EndFoldoutHeaderGroup();
		}

		private void DrawSettings_AppSettings ()
		{
			GUILayout.Space(2f);

			using (var cc = new EditorGUI.ChangeCheckScope())
			{
				var Sett = EditorGUILayout.BeginFoldoutHeaderGroup(_Settings_AppSettings_Open, "App Settings", FoldoutHeader_def);
				if (cc.changed)
				{
					_Settings_AppSettings_Open = Sett;
				}
			}
			EditorGUILayout.BeginVertical(Style_Box);

			EditorGUILayout.LabelField("App Settings Asset");
			int id = GUIUtility.GetControlID(FocusType.Keyboard);

			using (var cc = new EditorGUI.ChangeCheckScope())
			{
				var a1 = EditorGUILayout.ObjectField(settingsAsset.AppSettings, typeof(AppSettings), false) as AppSettings;
				if (cc.changed)
				{
					settingsAsset.AppSettings = a1;
				}
			}

			AppSettingsGUI.AutoRightClickMenu(settingsAsset, "AppSettings", Event.current.GetTypeForControl(id), true);
			#region Setup
			if (settingsAsset.AppSettings == null)
			{
				if (GUILayout.Button("Create"))
				{
					settingsAsset.AppSettings = AssetDatabase.LoadAssetAtPath<AppSettings>(CreateAsset<AppSettings>(DT_Asset_AppSettings_Path, DT_Asset_AppSettings_File));
					UpdateInformation();
				}
			}
			else
			{
				if (AppSettings.ActiveAsset != settingsAsset.AppSettings)
				{
					AppSettings.ActiveAsset = settingsAsset.AppSettings;
					UpdateInformation();
					DrawAppSettingsWarnningErrors(false);
				}
			}
			#endregion
			if (_Settings_AppSettings_Open)
			{
				if (settingsAsset.AppSettings != null)
				{
					using (var cc = new EditorGUI.ChangeCheckScope())
					{
						GUILayout.Space(5f);
						bool fieldEmpty;

						fieldEmpty = CheckFieldEmpty(settingsAsset.AppSettings.Info_AppName);
						UI_HightlightBackground_Start(fieldEmpty, Color.red, Color.red);
						var a1 = EditorGUILayout.TextField(new GUIContent("App Name"), settingsAsset.AppSettings.Info_AppName);
						UI_HightlightBackground_End();

						fieldEmpty = CheckFieldEmpty(settingsAsset.AppSettings.Info_CompanyName);
						UI_HightlightBackground_Start(fieldEmpty, Color.red, Color.red);
						var a3 = EditorGUILayout.TextField(new GUIContent("Company Name"), settingsAsset.AppSettings.Info_CompanyName);
						UI_HightlightBackground_End();

						fieldEmpty = CheckFieldEmpty(settingsAsset.AppSettings.Info_Copyright);
						UI_HightlightBackground_Start(fieldEmpty, Color.red, Color.red);
						var a2 = EditorGUILayout.TextField(new GUIContent("Copyright"), settingsAsset.AppSettings.Info_Copyright);
						UI_HightlightBackground_End();

						fieldEmpty = CheckFieldEmpty(settingsAsset.AppSettings.Build_EXEName);
						UI_HightlightBackground_Start(fieldEmpty, Color.red, Color.red);
						var a4 = EditorGUILayout.TextField(new GUIContent("EXE Name"), settingsAsset.AppSettings.Build_EXEName);
						UI_HightlightBackground_End();

						var a5 = EditorGUILayout.Toggle(new GUIContent("Force Single Instance"), settingsAsset.AppSettings.Settings_SingleInstance);

						GUILayout.BeginHorizontal();
						GUILayout.Label(new GUIContent("Version Min. Digits"));
						var a6 = settingsAsset.AppSettings.Ver_MinMajorDigits = EditorGUILayout.IntField(settingsAsset.AppSettings.Ver_MinMajorDigits, GUILayout.Width(20f));
						EditorGUILayout.LabelField(".", GUILayout.Width(5f));
						var a7 = EditorGUILayout.IntField(settingsAsset.AppSettings.Ver_MinMinorDigits, GUILayout.Width(20f));
						EditorGUILayout.LabelField(".", GUILayout.Width(5f));
						var a8 = EditorGUILayout.IntField(settingsAsset.AppSettings.Ver_MinPatchDigits, GUILayout.Width(20f));
						GUILayout.EndHorizontal();
						GUILayout.Space(5f);

						GUILayout.BeginHorizontal();
						EditorGUILayout.PrefixLabel("Build Path");
						GUILayout.FlexibleSpace();
						DrawBrowseButton(ref settingsAsset.AppSettings.Build_RootPath, false, false, "Choose Build Path");
						GUILayout.EndHorizontal();

						EditorGUILayout.HelpBox("Current Path:\n" + settingsAsset.AppSettings.Build_RootPath, string.IsNullOrEmpty(settingsAsset.AppSettings.Build_RootPath) ? MessageType.Error : MessageType.None);

						if (cc.changed)
						{
							settingsAsset.AppSettings.Info_AppName = a1;
							settingsAsset.AppSettings.Info_Copyright = a2;
							settingsAsset.AppSettings.Info_CompanyName = a3;
							settingsAsset.AppSettings.Build_EXEName = a4;
							settingsAsset.AppSettings.Settings_SingleInstance = a5;
							settingsAsset.AppSettings.Ver_MinMajorDigits = a6;
							settingsAsset.AppSettings.Ver_MinMinorDigits = a7;
							settingsAsset.AppSettings.Ver_MinPatchDigits = a8;
							settingsAsset.AppSettings.Apply();
						}
					}
				}
				else
				{
					EditorGUILayout.LabelField("Please select App Settings asset");
				}
			}

			EditorGUILayout.EndVertical();
			EditorGUILayout.EndFoldoutHeaderGroup();
		}

		private Color? Color_OriginalBackground;
		private Color? Color_OriginalTint;

		private void UI_HightlightBackground_Start (bool fieldEmpty, Color? backgroundColor = null, Color? tintColor = null)
		{
			Color_OriginalBackground = GUI.backgroundColor;
			Color_OriginalTint = GUI.color;

			if (backgroundColor.HasValue)
			GUI.backgroundColor = fieldEmpty ? 
				(backgroundColor ?? Color.black) : (Color_OriginalBackground ?? Color.black);

			if (tintColor.HasValue)
				GUI.color = fieldEmpty ? 
				(tintColor ?? Color.black) : (Color_OriginalTint ?? Color.black); 
		}

		private void UI_HightlightBackground_End ()
		{
			if (Color_OriginalBackground.HasValue)
				GUI.backgroundColor = Color_OriginalBackground ?? Color.black;

			if (Color_OriginalTint.HasValue)
				GUI.color = Color_OriginalTint?? Color.black;

			Color_OriginalBackground = null;
			Color_OriginalTint = null;
		}

		private bool CheckFieldEmpty (string fieldString)
		{
			return String.IsNullOrEmpty(fieldString) || String.IsNullOrWhiteSpace(fieldString);
		}

		private void DrawSettingsSceneShortcuts ()
		{
			EditorGUILayout.Space();
			using (new EditorGUILayout.VerticalScope())
			{
				_Settings_SceneShortcuts_Fold.target = EditorGUILayout.Foldout(_Settings_SceneShortcuts_Fold.target, "Scenes Shortcut Settings " + (settingsAsset.showSceneShortcuts? "(On)" : "(Off)"), true, FoldoutHeader_def);
				using (var f = new EditorGUILayout.FadeGroupScope(_Settings_SceneShortcuts_Fold.faded))
				{
					if (f.visible)
					{
						using (var v = new EditorGUILayout.VerticalScope(Style_Box))
						{
							_Settings_SceneShortcuts_Launch_Fold.target = EditorGUILayout.Foldout(_Settings_SceneShortcuts_Launch_Fold.target, "Quick Launch Scenes", true, FoldoutHeader_def);
							using (var f2 = new EditorGUILayout.FadeGroupScope(_Settings_SceneShortcuts_Launch_Fold.faded))
							{
								if (f2.visible)
								{
									EditorGUI.BeginDisabledGroup(!settingsAsset.showSceneShortcuts);
									DrawList(null, ref settingsAsset.launchSceneShortcuts,
											 drawListItem: (scene, last) => DrawScenePathField(scene, last),
											 createListItem: () => "Assets/Scenes/");
									EditorGUI.EndDisabledGroup();
								}
							}

							GUILayout.Space(2f);

							_Settings_SceneShortcuts_Load_Fold.target = EditorGUILayout.Foldout(_Settings_SceneShortcuts_Load_Fold.target, "Quick Load Scenes", true, FoldoutHeader_def);
							using (var f2 = new EditorGUILayout.FadeGroupScope(_Settings_SceneShortcuts_Load_Fold.faded))
							{
								if (f2.visible)
								{
									EditorGUI.BeginDisabledGroup(!settingsAsset.showSceneShortcuts);
									DrawList(null, ref settingsAsset.loadSceneShortcuts,
										 drawListItem: (scene, last) => DrawScenePathField(scene, last),
										 createListItem: () => "Assets/Scenes/");
									EditorGUI.EndDisabledGroup();
								}
							}
						}
					}
				}
			}
		}

		private void DrawSettingsSceneListMenu_Launch(Rect position)
		{
			DrawSettingsSceneListMenu(position, true);
		}

		private void DrawSettingsSceneListMenu_Load (Rect position)
		{
			DrawSettingsSceneListMenu(position, false);
		}

		private void DrawSettingsSceneListMenu(Rect position, bool Launch)
		{
			var menu = new GenericMenu();
			menu.AddItem(new GUIContent("Clear List"), false, OnItemClicked);
			menu.DropDown(new Rect());

			void OnItemClicked ()
			{
				if (Launch)
					settingsAsset.launchSceneShortcuts.Clear();
				else
					settingsAsset.loadSceneShortcuts.Clear();
			}
		}

		private string DrawScenePathField(string path, bool last)
        {
            EditorGUILayout.BeginVertical();
			string newPath;
			SceneAsset oldScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(path);
			SceneAsset newScene = EditorGUILayout.ObjectField(oldScene, typeof(SceneAsset), false) as SceneAsset;
			if (oldScene != newScene)
			{
				newPath = AssetDatabase.GetAssetPath(newScene);
			}
			else
			{
				newPath = path;
			}
			
			bool isSceneValid = File.Exists(newPath);
			if (!isSceneValid)
			{
				newPath = "";
			}

			if (!last)
			{
				GUILayout.Space(settingsAsset.Settings_SceneLists_SpaceAfterItem);
			}
			EditorGUILayout.EndVertical();
            return newPath;
        }
		#endregion
		#region General
		private void DrawAppSettingsWarnningErrors (bool ShowAppSettingsPrefix)
		{
			MessageType messageType = settingsAsset.showQuickBuild ? MessageType.Error : MessageType.Warning;
			string message = ShowAppSettingsPrefix? "AppSettings: " : "";

			if (string.IsNullOrEmpty(settingsAsset.AppSettings.Build_RootPath))
			{
				message += "\nBuild Path not set!";
			}

			if (string.IsNullOrEmpty(settingsAsset.AppSettings.Build_EXEName))
			{
				message += "\nEXE Name not set!";
			}

			if (string.IsNullOrEmpty(settingsAsset.AppSettings.Info_AppName))
			{
				message += "\nProduct Name not set!";
			}

			if (string.IsNullOrEmpty(settingsAsset.AppSettings.Info_CompanyName))
			{
				message += "\nCompany Name not set!";
			}

			if (string.IsNullOrEmpty(settingsAsset.AppSettings.Info_Copyright))
			{
				message += "\nCopyright not set!";
			}

			EditorGUILayout.HelpBox(message, messageType);
			if (GUILayout.Button("Solve"))
			{
				settingsAsset._Settings_AppSettings_Open = true;
				Window_State_InSettingsPanel = true;
			}
		}
		#endregion
		#endregion
		#region Helpers
		private GUIStyle Style_TextField(bool fieldEmpty = false)
		{
			if (!fieldEmpty)
			{
				return Style_TextField_Normal;
			}
			else
			{
				return Style_TextField_Error;
			}
		}

		private DynaToolsSettings LoadSettings ()
		{
			if (!File.Exists(DT_Asset_Settings_Path + DT_Asset_Settings_File))
			{
				CreateAsset<DynaToolsSettings>(DT_Asset_Settings_Path, DT_Asset_Settings_File);
			}
			DynaToolsSettings settings = AssetDatabase.LoadAssetAtPath(DT_Asset_Settings_Path + DT_Asset_Settings_File, typeof(DynaToolsSettings)) as DynaToolsSettings;
			UpdateInformation(settings);

			return settings;
		}

		private void UpdateInformation (DynaToolsSettings settings = null)
		{
			if (settings == null)
			{
				settings = settingsAsset;
				if (settings == null)
				{
					return;
				}
			}
			settings.UpdateAddonInfo();

			if (settings.AppSettings != null)
			{
				settings.AppSettings.UpdateAddonInfo();
			}
		}

		private void DrawList<T>(string title, ref List<T> list, Func<T,bool,T> drawListItem, Func<T> createListItem)
        {
			using (var v = new EditorGUILayout.VerticalScope(Style_Box))
			{
				if (!string.IsNullOrWhiteSpace(title) & !string.IsNullOrEmpty(title))
				{
					EditorGUILayout.LabelField(title);
				}

				GUILayout.Space(3f);

				for (int i = 0; i < list.Count; i++)
				{
					using (var h = new EditorGUILayout.HorizontalScope())
					{
						list[i] = drawListItem(list[i], (i == list.Count - 1));
						if (GUILayout.Button("x", Style_MiniButton, GUILayout.Width(17f), GUILayout.Height(17f)))
						{
							list.RemoveAt(i);
						}
					}
				}

				GUILayout.Space(3f);

				using (new EditorGUILayout.HorizontalScope())
				{
					GUILayout.FlexibleSpace();
					if (GUILayout.Button("+", Style_MiniButton, GUILayout.Width(17f), GUILayout.Height(17f)))
					{
						list.Add(createListItem());
					}
				}

				GUILayout.Space(1f);
			}
        }

		private string CreateAsset<T> (string directory, string name, bool failIfExists = false, bool overwrite = false) where T : ScriptableObject
		{
			var conf = ScriptableObject.CreateInstance<T>();

			if (!Directory.Exists(directory))
			{
				Directory.CreateDirectory(directory);
			}
			string tempName = name;

			if (!name.EndsWith(".asset"))
				name = name + ".asset";

			if (!File.Exists(directory + name))
			{ 
				AssetDatabase.CreateAsset(conf, directory + name);
				AssetDatabase.Refresh();
			}
			else
			{
				try
				{
					T test = AssetDatabase.LoadAssetAtPath<T>(directory + name);
					if (test.GetType() != typeof(T))
					{
						throw new Exception();
					}
				}
				catch
				{
					if (failIfExists)
					{
						if (File.Exists(directory + name))
						{
							return "Exists";
						}
					}
					if (!overwrite)
					{
						while (File.Exists(directory + name))
						{
							name = "_" + name;
						}
					}
					AssetDatabase.CreateAsset(conf, directory + name);
					AssetDatabase.Refresh();
				}
			}
            
            EditorUtility.FocusProjectWindow();
			return directory + name;
		}

        private string GetCurrentState()
        {
            string state = "Idle";
            Style_EditorStateLabel.normal.textColor = Color.green;
			Style_EditorStateLabel.fontStyle = FontStyle.Normal;
			Style_EditorStateLabel.fontSize = 32;
			Style_EditorStateLabel.alignment = TextAnchor.MiddleCenter;

			if (EditorApplication.isCompiling)
            {
                state = "Compiling";
                Style_EditorStateLabel.normal.textColor = Color.red;
				Style_EditorStateLabel.fontStyle = FontStyle.Bold;
				Style_EditorStateLabel.fontSize = 24;
			}
            else if (EditorApplication.isPaused)
            {
                state = "Paused";
                Style_EditorStateLabel.normal.textColor = Color.yellow;
				Style_EditorStateLabel.fontSize = 24;
			}
            else if (EditorApplication.isPlaying)
            {
                state = "Playing";
                Style_EditorStateLabel.normal.textColor = Color.blue;
				Style_EditorStateLabel.fontSize = 24;
			}
            else if (EditorApplication.isUpdating)
            {
                state = "Updating\nAssetDatabase";
                Style_EditorStateLabel.normal.textColor = Color.red;
				Style_EditorStateLabel.fontStyle = FontStyle.Bold;
				Style_EditorStateLabel.fontSize = 18;
			}

			if (settingsAsset != null && settingsAsset.showQuickShortcuts)
			{
				Style_EditorStateLabel.fontSize -= 7;
				if (settingsAsset.LoadPreloadScene)
				{
					Style_EditorStateLabel.fontSize -= 0;
				}
			}

			return state;
        }

		private string Number2String(int number, bool isCaps)
		{
			char c = (char)((isCaps ? 65 : 97) + (number - 1));
			return c.ToString();
		}

		private void CheckAndAddLoadPreloadScene ()
		{
			if (settingsAsset.LoadPreloadScene)
			{
				CheckAndAddLoadScene(settingsAsset._preloadScene);
			}
		}

		private void CheckAndAddLoadScene (string path)
		{
			var openScenes = EditorSceneManager.sceneCount;
			bool SceneIsLoaded = false;
			Scene target = EditorSceneManager.GetSceneByPath(path);
			for (int i = 0; i < openScenes; i++)
			{
				if (EditorSceneManager.GetSceneAt(i) == target)
				{
					SceneIsLoaded = true;
					continue;
				}
			}

			if (!SceneIsLoaded)
			{
				EditorSceneManager.OpenScene(path, OpenSceneMode.Additive);
			}
		}

		private void CheckAndLoadScene (string path)
		{
			var openScenes = EditorSceneManager.sceneCount;
			bool SceneIsLoaded = false;
			Scene target = EditorSceneManager.GetSceneByPath(path);
			for (int i = 0; i < openScenes; i++)
			{
				if (EditorSceneManager.GetSceneAt(i) == target)
				{
					SceneIsLoaded = true;
					continue;
				}
			}

			if (!SceneIsLoaded)
			{
				EditorSceneManager.OpenScene(path, OpenSceneMode.Single);
			}
		}

		private void CheckAndUnloadPreloadScene ()
		{
			if (settingsAsset.LoadPreloadScene)
			{
				CheckAndUnloadScene(settingsAsset._preloadScene);
			}
		}

		private void CheckAndUnloadScene (string path)
		{
			if (settingsAsset.LoadPreloadScene)
			{
				var openScenes = EditorSceneManager.sceneCount;
				bool SceneIsLoaded = false;
				Scene target = EditorSceneManager.GetSceneByPath(path);
				for (int i = 0; i < openScenes; i++)
				{
					if (EditorSceneManager.GetSceneAt(i) == target)
					{
						SceneIsLoaded = true;
						continue;
					}
				}

				if (SceneIsLoaded && openScenes > 1)
				{
					Scene[] targets = new Scene[1]{target };
					EditorSceneManager.SaveModifiedScenesIfUserWantsTo(targets);
					EditorSceneManager.CloseScene(target, true);
				}
			}
		}

		private void DrawBrowseButton (ref string Target, bool Local, bool File, string Title, string Format = "unity")
		{
			string controlName = "button" + UnityEngine.Random.Range(10000, 99999).ToString() + UnityEngine.Random.Range(10000, 99999).ToString();
			GUI.SetNextControlName(controlName);
			if (GUILayout.Button("Browse", GUILayout.Width(53f)))
			{
				Target = FileBrowser_Open(Local, File, Title, Format);
				EditorGUI.FocusTextInControl(controlName);
			}
		}

		private static string FileBrowser_Open (bool Local, bool File, string Title, string Format)
		{
			string Target;
			string newPath;
			if (File)
				newPath = EditorUtility.OpenFilePanel(Title, "", Format);
			else
				newPath = EditorUtility.OpenFolderPanel(Title, "", "");
			Target = DynaZor.DynaTools.Helpers.HandleFilePath(newPath, Local, File);
			return Target;
		}

		private static string FileBrowser_Save (bool Local, bool File, string Title, string Format)
		{
			string Target;
			string newPath;
			if (File)
				newPath = EditorUtility.SaveFilePanel(Title, "", "", Format);
			else
				newPath = EditorUtility.SaveFolderPanel(Title, "", "");
			Target = Helpers.HandleFilePath(newPath, Local, File);
			return Target;
		}

		private string GetBuildPath (bool Browse, out string folderName, bool ForceNoBrowse = false)
		{
			folderName = "";
			if (!Browse & (settingsAsset.AppSettings == null || (settingsAsset.AppSettings != null && String.IsNullOrEmpty(settingsAsset.AppSettings.Build_RootPath))))
			{
				if (ForceNoBrowse)
				{
					return null;
				}

				if (settingsAsset.AppSettings != null)
				{
					DTEditorOnly.EditorHelpers.DebugWarning("Build Path not set in AppSettings!");
				}
				Browse = true;
			}

			string calcBuildPath = Browse? FileBrowser_Save(false, true, "Select Build Path", "exe") : settingsAsset.AppSettings.Build_RootPath;
			string finalPath;
			if (!Browse)
			{
				// Set current data to date
				string date = DateTime.Now.ToString("yyyyMMdd");
				// Folder Name
				string calcBuildPath_Folder = "";
				switch (settingsAsset.AppSettings.Build_NameFormat)
				{
					case AppSettings.BuildNameFormat.Date:
						calcBuildPath_Folder = date;
						break;

					case AppSettings.BuildNameFormat.Date_Version:
						calcBuildPath_Folder = date + "_" + AppSettings.VersionToString(settingsAsset.AppSettings, false);
						break;

					case AppSettings.BuildNameFormat.State_Version:
						calcBuildPath_Folder = settingsAsset.AppSettings.Ver_AppDevStage
							+ "_" + AppSettings.VersionToString(settingsAsset.AppSettings, false);
						break;

					case AppSettings.BuildNameFormat.Version:
						calcBuildPath_Folder = AppSettings.VersionToString(settingsAsset.AppSettings, false);
						break;

					case AppSettings.BuildNameFormat.Version_Date:
						calcBuildPath_Folder = AppSettings.VersionToString(settingsAsset.AppSettings, false) + "_" + date;
						break;
				}

				string originalPath = calcBuildPath;
				string originalPath_Folder = calcBuildPath_Folder;
				int letterIndex = 0;
				calcBuildPath = Path.Combine(originalPath, calcBuildPath_Folder);
				while (Directory.Exists(calcBuildPath))
				{
					letterIndex++;
					calcBuildPath_Folder = originalPath_Folder + Number2String(letterIndex, true);
					calcBuildPath = Path.Combine(originalPath, calcBuildPath_Folder);
				}
				folderName = calcBuildPath_Folder;

				finalPath = Path.Combine(calcBuildPath, settingsAsset.AppSettings.Build_EXEName + ".exe");
			}
			else
			{
				folderName = Path.GetDirectoryName(calcBuildPath);
				finalPath = calcBuildPath;
			}
			return finalPath;
		}
		#endregion
	}
}
#endif