﻿// Library made by Shahar DynaZor Alon, please give credit if used in a project.
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace DynaZor.DynaTools
{
	public static class DTExtensions
	{
#if UNITY_EDITOR
		// Gets value from SerializedProperty - even if value is nested
		public static object GetValue (this UnityEditor.SerializedProperty property)
		{
			object obj = property.serializedObject.targetObject;
			foreach (var path in property.propertyPath.Split('.'))
			{
				var type = obj.GetType();
				FieldInfo field = type.GetField(path);
				if (field == null)
				{
					Helpers.DebugLog(property.name + " variable not found in " + ((GameObject) obj).name + "\nMake sure the variable is public!", obj as Object);
					continue;
				}
				obj = field.GetValue(obj);
			}
			
			return obj;
		}

		// Sets value from SerializedProperty - even if value is nested
		public static void SetValue (this UnityEditor.SerializedProperty property, object val)
		{
			object obj = property.serializedObject.targetObject;

			List<KeyValuePair<FieldInfo, object>> list = new List<KeyValuePair<FieldInfo, object>>();
			foreach (var path in property.propertyPath.Split('.'))
			{
				var type = obj.GetType();
				FieldInfo field = type.GetField(path);
				if (field == null)
				{
					Helpers.DebugWarning("\"" + property.name + "\" public variable not found in \"" + 
						property.serializedObject.targetObject.name + "\"\nMake sure the variable is public!", property.serializedObject.targetObject);
					continue;
				}
				list.Add(new KeyValuePair<FieldInfo, object>(field, obj));
				obj = field.GetValue(obj);
			}

			// Now set values of all objects, from child to parent
			for (int i = list.Count - 1; i >= 0; --i)
			{
				list[i].Key.SetValue(list[i].Value, val);
				// New 'val' object will be parent of current 'val' object
				val = list[i].Value;
			}
		}
#endif
	}
}