﻿// Custom version by Shahar DynaZor Alon, please give credit if used in a project.
#if UNITY_EDITOR
using UnityEngine;
using System.Collections.Generic;

namespace DynaZor.DynaTools.DTScriptableObjects.DynaToolsSettings
{
	[System.Serializable]
	public class DynaToolsSettings : ScriptableObject
	{
		#region AddonInformation
		public string AddonName = AddonInformation.AddonName;
		public string AddonVersion = AddonInformation.AddonVersion;
		public string AddonBy = AddonInformation.AddonBy;
		#endregion

		#region References
		public AppSettings AppSettings;
		#endregion

		#region Settings
		public bool showQuickBuild = true;
		public bool showQuickBuildSettings = true;
		public bool showEditorState = true;
		public bool showSceneShortcuts = true;
		public bool showQuickShortcuts = true;
		public bool showInfoSettings = true;
		public bool showLastBuild = true;
		public bool showVersion = true;
		public bool LoadPreloadScene = false;
		public bool _Settings_MoreInfo_Open = false;
		public string _preloadScene;
		public bool _setPlayScene = false;
		public string _playScene;
		public List<string> launchSceneShortcuts = new List<string>();
		public List<string> loadSceneShortcuts = new List<string>();
		#region Quick Build Settings
		[System.Flags]
		public enum BuildSettingsType
		{
			None = 0,
			AlwaysDev = 1,
			AutoDev = 2,
			OpenFolder = 4,
			All = ~0
		}
		public BuildSettingsType BuildSettings = BuildSettingsType.OpenFolder | BuildSettingsType.AutoDev;
		public bool showQuickBuildTarget = true;
		public UnityEditor.BuildOptions selectedBuildOptions = UnityEditor.BuildOptions.CompressWithLz4HC;
		public UnityEditor.BuildTarget selectedBuildTarget = UnityEditor.BuildTarget.StandaloneWindows64;
		#endregion
		#endregion

		#region UI
		public bool _Settings_AddonInfo_Open = true;
		public bool _Settings_AppSettings_Open = true;
		public bool _Settings_QuickBuild_Open = true;
		public bool _Settings_Preload_Open = true;
		#endregion

		#region Advanced Settings
		public bool showAdvancedSettings = false;
		public bool additiveLoadSceneWhilePlaying = false;
		public float Panel_Width_Min = 250f;
		public float Panel_Height_Min = 95f;
		public float SceneShortcuts_Height_Min = 50f;
		public float Button_LaunchScene_Width_Min = 90f;
		public float Button_LoadScene_Width_Min = 90f;
		public float Button_LoadSceneAdditive_Width_Min = 20f;
		public float Button_LoadSceneAdditive_Width_Max = 35f;
		public float SceneLists_Width_Max = 500f;
		public float Settings_Advanced_LabelWidth = 250f;
		public float VersionField_Width = 35f;
		public float Info_Field_Width_Min = 60f;
		public float Info_Field_Width_Max = 180f;
		public float Button_Quick_Play_Width = 55f;
		public float Button_Quick_Preload_Width = 55f;
		public float Settings_SceneLists_SpaceAfterItem = 8f;
		#endregion

		#region Events
		public void UpdateAddonInfo ()
		{
			AddonName = AddonInformation.AddonName;
			AddonVersion = AddonInformation.AddonVersion;
			AddonBy = AddonInformation.AddonBy;
		}
		#endregion
	}
}
#endif