﻿// Library made by Shahar DynaZor Alon, please give credit if used in a project.
#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

namespace DynaZor.DynaTools.DTEditorOnly
{
	///<summary>
	///Helpers accessible in Edit mode
	///For Runtime and Play Helpers, use EditorUtilities.Helpers
	///</summary>
	public static class EditorHelpers
	{
		public static class EditorUI
		{
			public static void DrawUILine (Color color, int thickness = 2, int padding = 10)
			{
				Rect r = EditorGUILayout.GetControlRect(GUILayout.Height(padding+thickness));
				r.height = thickness;
				r.y += padding / 2;
				r.x -= 2;
				r.width += 6;
				EditorGUI.DrawRect(r, color);
			}
		}
		///<summary>
		///For logging as the DynaTools. 
		///Don't use outside DynaTools to prevent confusion!
		///</summary>
		public static void DebugLog (string message, UnityEngine.Object obj = null)
		{
			Debug.Log(AddonInformation.AddonName + " - " + message, obj);
		}

		///<summary>
		///For logging as the DynaTools. 
		///Don't use outside DynaTools to prevent confusion!
		///</summary>
		public static void DebugWarning (string message, UnityEngine.Object obj = null)
		{
			Debug.LogWarning(AddonInformation.AddonName + " - " + message, obj);
		}
	}
}
#endif