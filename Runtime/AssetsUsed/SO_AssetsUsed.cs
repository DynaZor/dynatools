﻿// Library made by Shahar DynaZor Alon, please give credit if used in a project.

using System.Collections.Generic;
using UnityEngine;

namespace DynaZor.EditorUtilities.AssetsUsed
{
	public class SO_AssetsUsed : ScriptableObject
	{
		public List<AssetInfo> Assets = new List<AssetInfo>();

		#region Constructors
		public SO_AssetsUsed () { }
		public SO_AssetsUsed (SO_AssetsUsed asset)
		{
			Apply(asset);
		}
		#endregion
		#region Actions
		public void Apply (SO_AssetsUsed asset)
		{
			Assets = new List<AssetInfo>();
			for (int i = 0; i < asset.Assets.Count; i++)
			{
				Assets.Add(new AssetInfo(asset.Assets[i]));
			}
		}
		#endregion
	}
}